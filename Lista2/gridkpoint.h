#ifndef GRIDKPOINT_H
#define GRIDKPOINT_H

#include "point.h"
#include "polygon.h"

class GridKPoint : public KPoint
{
public:
    GridKPoint(Polygon polygon);
    ~GridKPoint() = default;

    Eigen::Vector2d B1;
    Eigen::Vector2d B2;
    double DistanceDivider = 100;

    std::vector<GridKPoint> Neighbors;
};

#endif // GRIDKPOINT_H
