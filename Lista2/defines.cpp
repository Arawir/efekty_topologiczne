#include "defines.h"

PauliMatrix SigmaX{0.0,1.0,1.0,0.0};
PauliMatrix SigmaY{0.0,-i,i,0.0};
PauliMatrix SigmaZ{1.0,0.0,0.0,-1.0};
PauliMatrix I{1.0,0.0,0.0,1.0};
