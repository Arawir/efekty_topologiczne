#ifndef POLYGON_H
#define POLYGON_H

#include <vector>
#include <iostream>

struct Pointu{
    Pointu(double x, double y) :
        X(x)
      , Y(y)
    {

    }
    double X,Y;
};

class Polygon{
public:
    Polygon();
    void addPoint(Pointu newPoint);
    bool pointIsInside(Pointu pointToCheck);
    double maxX();
    double minX();
    double maxY();
    double minY();
public:
    double sign(Pointu a, Pointu b, Pointu c);
    bool pointIsInTriangle(Pointu p, Pointu a, Pointu b, Pointu c);
    std::vector<Pointu> Points;
    double MaxX, MinX, MaxY, MinY;

};

std::ostream& operator<< (std::ostream& stream, const Polygon& polygon);

#endif // POLYGON_H
