#ifndef DEFINES
#define DEFINES

#include "paulimatrix.h"

#define complex std::complex<double>
#define i complex{0,1}
#define EigenSolver Eigen::SelfAdjointEigenSolver<Eigen::Matrix3cd>

extern PauliMatrix SigmaX;
extern PauliMatrix SigmaY;
extern PauliMatrix SigmaZ;
extern PauliMatrix I;


#endif // DEFINES

