TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    polygon.cpp \
    defines.cpp \
    point.cpp \
    phasepoint.cpp \
    pointmap.cpp \
    gridkpoint.cpp

HEADERS += \
    polygon.h \
    defines.h \
    paulimatrix.h \
    point.h \
    phasepoint.h \
    pointmap.h \
    gnuplot.h \
    gridkpoint.h

