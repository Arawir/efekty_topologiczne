#include "point.h"
#include <cmath>
#include <fstream>
#include "polygon.h"
#include "phasepoint.h"

#include "gnuplot.h"



Polygon* prepareFirstBrillouinZone()
{
    std::fstream file;
    file.open("../data/FBZ.dat", std::ios::out);

    Polygon* FBZ = new Polygon{};
    double rKfbz = sqrt(16.0*M_PI*M_PI/27.0);
    double rMfbz = sqrt(4.0*M_PI*M_PI/9.0);

    FBZ->addPoint({rKfbz*cos(0.0*M_PI/3.0), rKfbz*sin(0.0*M_PI/3.0)});
    FBZ->addPoint({rMfbz*cos(0.5*M_PI/3.0), rMfbz*sin(0.5*M_PI/3.0)});
    FBZ->addPoint({rKfbz*cos(1.0*M_PI/3.0), rKfbz*sin(1.0*M_PI/3.0)});
    FBZ->addPoint({rMfbz*cos(1.5*M_PI/3.0), rMfbz*sin(1.5*M_PI/3.0)});
    FBZ->addPoint({rKfbz*cos(2.0*M_PI/3.0), rKfbz*sin(2.0*M_PI/3.0)});
    FBZ->addPoint({rMfbz*cos(2.5*M_PI/3.0), rMfbz*sin(2.5*M_PI/3.0)});
    FBZ->addPoint({rKfbz*cos(3.0*M_PI/3.0), rKfbz*sin(3.0*M_PI/3.0)});
    FBZ->addPoint({rMfbz*cos(3.5*M_PI/3.0), rMfbz*sin(3.5*M_PI/3.0)});
    FBZ->addPoint({rKfbz*cos(4.0*M_PI/3.0), rKfbz*sin(4.0*M_PI/3.0)});
    FBZ->addPoint({rMfbz*cos(4.5*M_PI/3.0), rMfbz*sin(4.5*M_PI/3.0)});
    FBZ->addPoint({rKfbz*cos(5.0*M_PI/3.0), rKfbz*sin(5.0*M_PI/3.0)});
    FBZ->addPoint({rMfbz*cos(5.5*M_PI/3.0), rMfbz*sin(5.5*M_PI/3.0)});

    file << *FBZ << std::endl;

    file.close();

    return FBZ;
}



void calculateEnergyBandsWiaMap(HaldlaneModel& model)
{
    std::fstream file;
    file.open("../data/Zad_1b.dat", std::ios::out);

    KPoint* Point[100][100];
    double x=-M_PI, y=-M_PI;

    for(int iter=0; iter<100; iter++){
        y =-M_PI;
        for(int jter=0; jter<100; jter++){
            Point[iter][jter] = new KPoint(x, y, model);
            y += 2.0*M_PI/100.0;
        }
        x += 2.0*M_PI/100.0;
    }

    for(int iter=0; iter<100; iter++){
        for(int jter=0; jter<100; jter++){
            file << *Point[iter][jter] << std::endl;
        }
        file << std::endl;
    }


    file.close();

    GnuplotPipe gp;
    gp.sendLine("set pm3d");

    gp.sendLine("set title font \"helvetica,20\"");
    gp.sendLine("set xlabel font \"helvetica,20\"");
    gp.sendLine("set ylabel font \"helvetica,20\"");
    gp.sendLine("set zlabel font \"helvetica,20\"");
    gp.sendLine("set xtics font \"helvetica,14\"");
    gp.sendLine("set ytics font \"helvetica,14\"");
    gp.sendLine("set ztics font \"helvetica,14\"");

    gp.sendLine("set title \"Enery bands in kagome lattice \\n t_1 = 1.0 | t_2 = -0.3 | {/Symbol l}_1 = 0.28 | {/Symbol l}_2 = 0.2\"");
    gp.sendLine("set xlabel \"k_x\"");
    gp.sendLine("set ylabel \"k_y\"");
    gp.sendLine("unset key");
    gp.sendLine("set border 127+256+512");
    gp.sendLine("set grid ztics");
    gp.sendLine("set grid");
    gp.sendLine("set ticslevel 0");

    gp.sendLine("set xrange [-pi:pi]");
    gp.sendLine("set xtics (\"0\" 0, \"{/Symbol P}\" pi, \"-{/Symbol P}\" -pi, \"{/Symbol P}/2\" pi/2, \"-{/Symbol P}/2\" -pi/2)");
    gp.sendLine("set yrange [-pi:pi]");
    gp.sendLine("set ytics (\"0\" 0, \"{/Symbol P}\" pi, \"-{/Symbol P}\" -pi, \"{/Symbol P}/2\" pi/2, \"-{/Symbol P}/2\" -pi/2)");

    gp.sendLine("splot \"../data/Zad_1b.dat\" u 1:2:3 w points pointtype 7 pointsize 0, "
                "\"../data/Zad_1b.dat\" u 1:2:4 w points pointtype 7 pointsize 0, "
                "\"../data/Zad_1b.dat\" u 1:2:5 w points pointtype 7 pointsize 0");

    gp.sendEndOfData(1);
    getchar();

}

void calculateEnergyBandsViaPath(HaldlaneModel& model){
    std::fstream file;
    file.open("../data/Zad_1bb.dat", std::ios::out);


    double PathLength = 0.0;
    double Pkx=0.0;
    double Pky =0.0;

    std::vector<KPoint> Points;
    double x, y, x0, y0;

    double Gx =0.0, Gy = 0.0, Kx = 4.0*M_PI/3.0/sqrt(3.0), Ky = 0.0, Mx=M_PI/sqrt(3.0), My = M_PI/3.0;

    double alpha = atan((Ky-Gy)/(Kx-Gx));
    double r = 0.0;
    double RMax = sqrt((Kx-Gx)*(Kx-Gx)+(Ky-Gy)*(Ky-Gy));
    x0 = Gx;
    y0 = Gy;
    while(r<RMax){
        x = r*cos(alpha)+x0;
        y = r*sin(alpha)+y0;
        Points.push_back(KPoint{x, y, model});
        r+= RMax/400.0;
    }

    alpha = atan((My-Ky)/(Mx-Kx));
    r = 0.0;
    RMax = sqrt((Mx-Kx)*(Mx-Kx)+(My-Ky)*(My-Ky));
    x0 = Kx;
    y0 = Ky;
    while(r<RMax){
        x = -r*cos(alpha)+x0;
        y = -r*sin(alpha)+y0;
        Points.push_back(KPoint{x, y, model});
        r+= RMax/400.0;
    }

    alpha = atan((Gy-My)/(Gx-Mx));
    r = 0.0;
    RMax = sqrt((Gx-Mx)*(Gx-Mx)+(Gy-My)*(Gy-My));
    x0 = Mx;
    y0 = My;
    while(r<RMax){
        x = -r*cos(alpha)+x0;
        y = -r*sin(alpha)+y0;
        Points.push_back(KPoint{x, y, model});
        r+= RMax/400.0;
    }

    for(KPoint &point : Points){
        file << PathLength << "      " << point << std::endl << std::endl;
        PathLength+= sqrt( (point.Kx-Pkx)*(point.Kx-Pkx)+(point.Ky-Pky)*(point.Ky-Pky) );
        Pkx = point.Kx;
        Pky = point.Ky;
    }


    file.close();
    GnuplotPipe gp;
    gp.sendLine("unset pm3d");
    gp.sendLine("set title font \"helvetica,20\"");
    gp.sendLine("set xlabel font \"helvetica,20\"");
    gp.sendLine("set ylabel font \"helvetica,20\"");
    gp.sendLine("set zlabel font \"helvetica,20\"");
    gp.sendLine("set xtics font \"helvetica,14\"");
    gp.sendLine("set ytics font \"helvetica,14\"");
    gp.sendLine("set ztics font \"helvetica,14\"");

    gp.sendLine("set title \"Enery bands via {/Symbol G}-K-M-{/Symbol G} path \\n\\n t_1 = 1.0 | t_2 = -0.3 | {/Symbol l}_1 = 0.28 | {/Symbol l}_2 = 0.2\"");
    gp.sendLine("set xlabel \"\"");
    gp.sendLine("set ylabel \"E\"");
    gp.sendLine("unset key");
    gp.sendLine("unset xlabelgp.sendLine("set xtics (\"{/Symbol G}\" 0, \"K\" 2.40631, \"M\" 3.62155, \"-{/Symbol G}\" 5.71676)");

    gp.sendLine("plot \"../data/Zad_1bb.dat\" u 1:4 w points pointtype 7 pointsize 1, "
                "\"../data/Zad_1bb.dat\" u 1:5 w points pointtype 7 pointsize 1, "
                "\"../data/Zad_1bb.dat\" u 1:6 w points pointtype 7 pointsize 1");

    gp.sendEndOfData(1);
    getchar();
}

void calculateBerryCurvature(HaldlaneModel& model){
    std::fstream file;
    file.open("../data/Zad_1d.dat", std::ios::out);

    KPoint* Point[100][100];
    std::vector<PhasePoint> PhasePoints;

    double x=-M_PI, y=-M_PI;

    for(int iter=0; iter<100; iter++){
        y =-M_PI;
        for(int jter=0; jter<100; jter++){
            Point[iter][jter] = new KPoint(x, y, model);
            y += 2.0*M_PI/100.0;
        }
        x += 2.0*M_PI/100.0;
    }

    for(int iter=0; iter<99; iter++){
        for(int jter=0; jter<99; jter++){
            PhasePoints.push_back(PhasePoint{});
            PhasePoints.back().addKPoint(Point[iter+0][jter+0]);
            PhasePoints.back().addKPoint(Point[iter+1][jter+0]);
            PhasePoints.back().addKPoint(Point[iter+1][jter+1]);
            PhasePoints.back().addKPoint(Point[iter+0][jter+1]);
            file << PhasePoints.back() << std::endl;
        }
        file << std::endl;
    }

    file.close();

    GnuplotPipe gp;
    gp.sendLine("set pm3d");

    gp.sendLine("set title font \"helvetica,20\"");
    gp.sendLine("set xlabel font \"helvetica,20\"");
    gp.sendLine("set ylabel font \"helvetica,20\"");
    gp.sendLine("set zlabel font \"helvetica,20\"");
    gp.sendLine("set xtics font \"helvetica,14\"");
    gp.sendLine("set ytics font \"helvetica,14\"");
    gp.sendLine("set ztics font \"helvetica,14\"");

    gp.sendLine("set title \"Berry curvature in kagome lattice \\n t_1 = 1.0 | t_2 = -0.3 | {/Symbol l}_1 = 0.28 | {/Symbol l}_2 = 0.2\"");
    gp.sendLine("set xlabel \"k_x\"");
    gp.sendLine("set ylabel \"k_y\"");
    gp.sendLine("unset key");
    gp.sendLine("set border 127+256+512");
    gp.sendLine("set grid ztics");
    gp.sendLine("set ticslevel 0");
    gp.sendLine("set grid");

    gp.sendLine("set xrange [-pi:pi]");
    gp.sendLine("set xtics (\"0\" 0, \"{/Symbol P}\" pi, \"-{/Symbol P}\" -pi, \"{/Symbol P}/2\" pi/2, \"-{/Symbol P}/2\" -pi/2)");
    gp.sendLine("set yrange [-pi:pi]");
    gp.sendLine("set ytics (\"0\" 0, \"{/Symbol P}\" pi, \"-{/Symbol P}\" -pi, \"{/Symbol P}/2\" pi/2, \"-{/Symbol P}/2\" -pi/2)");

    gp.sendLine("splot \"../data/Zad_1b.dat\" u 1:2:4 w points pointtype 7 pointsize 0");

    gp.sendEndOfData(1);
    getchar();
}

double calculateChern(HaldlaneModel& model, Polygon* fbz){
    KPoint* Point[100][100];
    std::vector<PhasePoint> PhasePoints;

    std::fstream file;
    file.open("../data/tm.dat", std::ios::out);

    double ChernNumber=0.0;

    double x=-M_PI, y=-M_PI;

    for(int iter=0; iter<100; iter++){
        y =-M_PI;
        for(int jter=0; jter<100; jter++){
            Point[iter][jter] = new KPoint(x, y, model);
            y += 2.0*M_PI/100.0;
        }
        x += 2.0*M_PI/100.0;
    }

    for(int iter=0; iter<99; iter++){
        for(int jter=0; jter<99; jter++){
            PhasePoint point{};
            point.addKPoint(Point[iter+0][jter+0]);
            point.addKPoint(Point[iter+1][jter+0]);
            point.addKPoint(Point[iter+1][jter+1]);
            point.addKPoint(Point[iter+0][jter+1]);

            if( fbz->pointIsInside({point.x(),point.y()}) ){
               file << point.x() << "   " << point.y() << "    "<< point.phase() << std::endl;
               ChernNumber += point.phase();
            }
        }
    }
    file.close();
    return ChernNumber/2.0/M_PI;
}


void calculateBerryCurvatureViaPath(HaldlaneModel& model){
    std::fstream file;
    file.open("../data/Zad_1dd.dat", std::ios::out);


    std::vector<PhasePoint> PhasePoints;
    std::vector<KPoint> KPoints;

    double x, y, x0, y0;

    double Gx =0.0, Gy = 0.0, Kx = 4.0*M_PI/3.0/sqrt(3.0), Ky = 0.0, Mx=sqrt(3.0)*M_PI/3.0, My = M_PI/3.0;

    double alpha = atan((Ky-Gy)/(Kx-Gx));
    double r = 0.0;
    double RMax = sqrt((Kx-Gx)*(Kx-Gx)+(Ky-Gy)*(Ky-Gy));
    x0 = Gx;
    y0 = Gy;
    while(r<RMax){
        x = r*cos(alpha)+x0;
        y = r*sin(alpha)+y0;
        PhasePoints.push_back(PhasePoint{});
        KPoints.push_back(KPoint{x+0.005,y+0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());
        KPoints.push_back(KPoint{x-0.005,y+0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());
        KPoints.push_back(KPoint{x-0.005,y-0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());
        KPoints.push_back(KPoint{x+0.005,y-0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());

        r+= RMax/100.0;
    }

    alpha = atan((My-Ky)/(Mx-Kx));
    r = 0.0;
    RMax = sqrt((Mx-Kx)*(Mx-Kx)+(My-Ky)*(My-Ky));
    x0 = Kx;
    y0 = Ky;
    while(r<RMax){
        x = -r*cos(alpha)+x0;
        y = -r*sin(alpha)+y0;
        PhasePoints.push_back(PhasePoint{});
        KPoints.push_back(KPoint{x+0.005,y+0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());
        KPoints.push_back(KPoint{x-0.005,y+0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());
        KPoints.push_back(KPoint{x-0.005,y-0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());
        KPoints.push_back(KPoint{x+0.005,y-0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());
        r+= RMax/100.0;
    }

    alpha = atan((Gy-My)/(Gx-Mx));
    r = 0.0;
    RMax = sqrt((Gx-Mx)*(Gx-Mx)+(Gy-My)*(Gy-My));
    x0 = Mx;
    y0 = My;
    while(r<RMax){
        x = -r*cos(alpha)+x0;
        y = -r*sin(alpha)+y0;
        PhasePoints.push_back(PhasePoint{});
        KPoints.push_back(KPoint{x+0.005,y+0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());
        KPoints.push_back(KPoint{x-0.005,y+0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());
        KPoints.push_back(KPoint{x-0.005,y-0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());
        KPoints.push_back(KPoint{x+0.005,y-0.005, model});
        PhasePoints.back().addKPoint(&KPoints.back());
        r+= RMax/100.0;
    }

    for(int iter=0; iter<300; iter++){
        file << PhasePoints[iter] << std::endl;
    }

    file.close();
}



int main(){
    Polygon* FBZ = prepareFirstBrillouinZone();
    HaldlaneModel Model;
    Model.t1 = 1.0;
    Model.t2 = -0.3;
    Model.lambda1 = 0.28;
    Model.lambda2 = 0.2;

    calculateEnergyBandsWiaMap(Model);
    calculateEnergyBandsViaPath(Model);
    calculateBerryCurvature(Model);
    std::cout << "ChN: " << calculateChern(Model, FBZ) << std::endl;

    return 0;
}

