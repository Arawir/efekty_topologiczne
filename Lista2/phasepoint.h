#ifndef PHASEPOINT_H
#define PHASEPOINT_H

#include "point.h"

class PhasePoint
{
public:
    PhasePoint();
    ~PhasePoint();
    void addKPoint(KPoint* kpoint);
    double phase();
    double phase2();
    double phase3();
    double x();
    double y();
    double phasePerSquare();
    double phasePerSquare2();
    double phasePerSquare3();
private:
    void calculateIfNeeded();
private:
    bool WosCalculated;
    double Phase;
    double Phase2;
    double Phase3;
    double Square;
    double X, Y;
    std::vector<KPoint*> KPoints;
};

std::ostream& operator<< (std::ostream& stream, PhasePoint& phasePoint);

#endif // PHASEPOINT_H
