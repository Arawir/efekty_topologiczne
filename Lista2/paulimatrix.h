#ifndef PAULIMATRIX_H
#define PAULIMATRIX_H

#include <Eigen/Dense>


class PauliMatrix : public Eigen::Matrix2cd
{
public:
    PauliMatrix(std::complex<double> aa, std::complex<double> ab, std::complex<double> ba, std::complex<double> bb) :
        Eigen::Matrix2cd{}
    {
        *this << aa, ab, ba, bb;
    }
    ~PauliMatrix() = default;
};

#endif // PAULIMATRIX_H
