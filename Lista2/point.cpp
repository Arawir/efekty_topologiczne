#include "point.h"
#include <iomanip>


KPoint::KPoint(double kx, double ky, HaldlaneModel &model) :
    Kx(kx)
  , Ky(ky)
  , Model(model)
{
    double k1 = Kx*(-sqrt(3.0)/2.0)+Ky*3.0/2.0;
    double k2 = Kx*(sqrt(3.0)/2.0)+Ky*3.0/2.0;
    double k3 = k1-k2;

    Eigen::Matrix3cd M1;
    M1(0,0) = 0.0;
    M1(0,1) = 1.0+exp(i*k1);
    M1(0,2) = 1.0+exp(i*k2);
    M1(1,0) = 1.0+exp(-i*k1);
    M1(1,1) = 0.0;
    M1(1,2) = 1.0+exp(-i*k3);
    M1(2,0) = 1.0+exp(-i*k2);
    M1(2,1) = 1.0+exp(i*k3);
    M1(2,2) = 0.0;

    Eigen::Matrix3cd M2;
    M2(0,0) = 0.0;
    M2(0,1) = 1.0+exp(i*k1);
    M2(0,2) = -(1.0+exp(i*k2));
    M2(1,0) = -(1.0+exp(-i*k1));
    M2(1,1) = 0.0;
    M2(1,2) = 1.0+exp(-i*k3);
    M2(2,0) = 1.0+exp(-i*k2);
    M2(2,1) = -(1.0+exp(i*k3));
    M2(2,2) = 0.0;

    Eigen::Matrix3cd M3;
    M3(0,0) = 0.0;
    M3(0,1) = exp(i*k2)+exp(i*k3);
    M3(0,2) = exp(i*k1)+exp(-i*k3);
    M3(1,0) = exp(-i*k2)+exp(-i*k3);
    M3(1,1) = 0.0;
    M3(1,2) = exp(-i*k1)+exp(i*k2);
    M3(2,0) = exp(-i*k1)+exp(i*k3);
    M3(2,1) = exp(i*k1)+exp(-i*k2);
    M3(2,2) = 0.0;

    Eigen::Matrix3cd M4;
    M4(0,0) = 0.0;
    M4(0,1) = -(exp(i*k2)+exp(i*k3));
    M4(0,2) = exp(i*k1)+exp(-i*k3);
    M4(1,0) = exp(-i*k2)+exp(-i*k3);
    M4(1,1) = 0.0;
    M4(1,2) = -(exp(-i*k1)+exp(i*k2));
    M4(2,0) = -(exp(-i*k1)+exp(i*k3));
    M4(2,1) = exp(i*k1)+exp(-i*k2);
    M4(2,2) = 0.0;



    Eigen::Matrix3cd H = -1*( Model.t1*M1 + i*Model.lambda1*M2 + Model.t2*M3 + i*Model.lambda2*M4);

    Model.Solver.compute(H);
    Eigenvalues = Model.Solver.eigenvalues();
    Eigenvectors = Model.Solver.eigenvectors();
}


std::ostream& operator<< (std::ostream& stream, const KPoint& kpoint) {
    stream << std::fixed << std::setprecision(5)
           << kpoint.Kx << "   "
           << kpoint.Ky << "   "
           << kpoint.Eigenvalues(0) << "   "
           << kpoint.Eigenvalues(1) << "   "
           << kpoint.Eigenvalues(2) << "   ";

    return stream;
}
