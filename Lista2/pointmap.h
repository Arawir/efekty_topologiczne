#ifndef POINTMAP_H
#define POINTMAP_H

#include "point.h"

class PointMap{
public:
    PointMap();
    ~PointMap() = default;


    KPoint* Point[100][100];
};

#endif // POINTMAP_H
