#include "polygon.h"
#include <iostream>
#include <iomanip>

Polygon::Polygon() :
    Points{}
{

}

void Polygon::addPoint(Pointu newPoint)
{
    Points.push_back(newPoint);
    if(Points.size()==1){
        MaxX = newPoint.X;
        MinX = newPoint.X;
        MaxY = newPoint.Y;
        MinY = newPoint.Y;
    } else {
        if(newPoint.X > MaxX) MaxX = newPoint.X;
        if(newPoint.X < MinX) MinX = newPoint.X;
        if(newPoint.Y > MaxY) MaxY = newPoint.Y;
        if(newPoint.Y < MinY) MinY = newPoint.Y;
    }
}

bool Polygon::pointIsInside(Pointu pointToCheck)
{
    if(Points.size()<3){
        return false;
    }

    for(unsigned int iter=1; iter<Points.size()-1; iter++){
        if( pointIsInTriangle(pointToCheck,Points[0],Points[iter],Points[iter+1])){
            return true;
        }
    }
    return false;
}

double Polygon::maxX()
{
    return MaxX;
}

double Polygon::minX()
{
    return MinX;
}

double Polygon::maxY()
{
    return MaxY;
}

double Polygon::minY()
{
    return MinY;
}

double Polygon::sign(Pointu a, Pointu b, Pointu c)
{
    return (a.X - c.X) * (b.Y - c.Y) - (b.X - c.X) * (a.Y - c.Y);
}

bool Polygon::pointIsInTriangle(Pointu p, Pointu a, Pointu b, Pointu c)
{
    double d1, d2, d3;
    bool has_neg, has_pos;

    d1 = sign(p, a, b);
    d2 = sign(p, b, c);
    d3 = sign(p, c, a);

    has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
    has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

//    std::cout << p.X << "  "
//              << p.Y << std::endl
//              << a.X << "  "
//              << a.Y << std::endl
//              << b.X << "  "
//              << b.Y << std::endl
//              << c.X << "  "
//              << c.Y << std::endl << std::endl;

    return !(has_neg && has_pos);
}

std::ostream &operator<<(std::ostream &stream, const Polygon &polygon)
{
    for(Pointu point : polygon.Points){
        stream << std::fixed << std::setprecision(5)
               << point.X << "   "
               << point.Y << "   "
               << std::endl;
    }
    return stream;
}
