#ifndef KPOINT_H
#define KPOINT_H

#include "defines.h"
#include <Eigen/Dense>
#include <iostream>
#include "polygon.h"
#include <cmath>

enum class GridType{
    Path,
    Grid4,
};

struct HaldlaneModel{
    double t1 =1.0;
    double t2 =1.0;
    double lambda1 = 0.5;
    double lambda2 = 0.5;
    EigenSolver Solver;
};

class KPoint
{
public:
    KPoint(double kx, double ky, HaldlaneModel& model);
    ~KPoint() = default;
public:
    double Kx, Ky;
    HaldlaneModel& Model;
    Eigen::Vector3d D;
    Eigen::VectorXd Eigenvalues;
    Eigen::MatrixXcd Eigenvectors;
};

std::ostream& operator<< (std::ostream& stream, const KPoint& kpoint);


#endif // POINT_H
