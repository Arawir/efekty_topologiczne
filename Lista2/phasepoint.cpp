#include "phasepoint.h"

PhasePoint::PhasePoint() :
    WosCalculated{false}
{

}

PhasePoint::~PhasePoint()
{
    KPoints.clear();
}

void PhasePoint::addKPoint(KPoint *kpoint)
{
    KPoints.push_back(kpoint);
}

double PhasePoint::phase()
{
    calculateIfNeeded();
    return Phase;
}

double PhasePoint::phase2()
{
    calculateIfNeeded();
    return Phase2;
}

double PhasePoint::phase3()
{
    calculateIfNeeded();
    return Phase3;
}

double PhasePoint::x()
{
    calculateIfNeeded();
    return X;
}

double PhasePoint::y()
{
    calculateIfNeeded();
    return Y;
}

double PhasePoint::phasePerSquare()
{
    calculateIfNeeded();
    return Phase/Square;
}

double PhasePoint::phasePerSquare2()
{
    calculateIfNeeded();
    return Phase2/Square;
}

double PhasePoint::phasePerSquare3()
{
    calculateIfNeeded();
    return Phase3/Square;
}

void PhasePoint::calculateIfNeeded()
{
    if(!WosCalculated){
        X = 0;
        Y = 0;
        complex tmp=1.0;
        complex tmp2=1.0;
        complex tmp3=1.0;
        Square = (KPoints[0]->Kx-KPoints[2]->Kx)*(KPoints[0]->Ky-KPoints[2]->Ky);
        if(Square<0){ Square = -Square; }
        for(KPoint* kpoint : KPoints){
            X+= kpoint->Kx;
            Y+= kpoint->Ky;
        }
        X=X/static_cast<double>(KPoints.size());
        Y=Y/static_cast<double>(KPoints.size());


        for(int iter=0; iter<KPoints.size()-1; iter++){
//            std::cout << KPoints[iter]->Eigenvectors.col(1) << std::endl;
//            tmp *= static_cast<complex>(KPoints[iter]->Eigenvectors.col(0).adjoint() * KPoints[iter+1]->Eigenvectors.col(0));
//            tmp2 *= static_cast<complex>(KPoints[iter]->Eigenvectors.col(1).adjoint() * KPoints[iter+1]->Eigenvectors.col(1));
//            tmp2 *= static_cast<complex>(KPoints[iter]->Eigenvectors.col(2).adjoint() * KPoints[iter+1]->Eigenvectors.col(2));
        }
        tmp *= static_cast<complex>(KPoints[KPoints.size()-1]->Eigenvectors.col(0).adjoint() * KPoints[0]->Eigenvectors.col(0));
        tmp2 *= static_cast<complex>(KPoints[KPoints.size()-1]->Eigenvectors.col(1).adjoint() * KPoints[0]->Eigenvectors.col(1));
        tmp2 *= static_cast<complex>(KPoints[KPoints.size()-1]->Eigenvectors.col(2).adjoint() * KPoints[0]->Eigenvectors.col(2));
        Phase = std::arg(tmp);
        Phase2 = std::arg(tmp2);
        Phase3 = std::arg(tmp3);


        WosCalculated = true;
    }

}


std::ostream &operator<<(std::ostream &stream, PhasePoint &phasePoint)
{
    stream << phasePoint.x() << "   "
           << phasePoint.y() << "   "
           << phasePoint.phase() << "   "
           << phasePoint.phase2() << "   "
           << phasePoint.phase3() << "   "
           << phasePoint.phasePerSquare() << "   "
           << phasePoint.phasePerSquare2() << "   ";

    return stream;
}
