TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    kpoint.cpp \
    phisicalmodel.cpp

HEADERS += \
    defines.h \
    kpoint.h \
    phisicalmodel.h

