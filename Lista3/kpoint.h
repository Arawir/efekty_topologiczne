#ifndef KPOINT_H
#define KPOINT_H

#include "phisicalmodel.h"


class KPoint
{
public:
    KPoint(double k, PhisicalModel& model);
    ~KPoint() = default;

    void generateHamiltonianWithZigZagEdge();
    void generateHamiltonianWithArmchairEdge();
    void calculate();
    friend std::ostream& operator<< (std::ostream& stream, const KPoint& point);
public:
    double K;
    PhisicalModel& Model;

    Eigen::VectorXd Eigenvalues;
    Eigen::MatrixXcd Eigenvectors;
};

std::ostream& operator<< (std::ostream& stream, const KPoint& point);

#endif // KPOINT_H
