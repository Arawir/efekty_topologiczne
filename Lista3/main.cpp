#include "defines.h"
#include "kpoint.h"
#include <fstream>
#include <vector>
#include "gnuplot.h"


std::vector<KPoint> generateKPointGrid(PhisicalModel& model){
    std::vector<KPoint> Points;
    double K=-0.1;
    while(K<=2.0*M_PI+0.1){
        Points.push_back( KPoint{K, model} );
        K+=M_PI/100;
    }
    return Points;
}

void setGnuplotData(GnuplotPipe& gp, PhisicalModel& model){
    std::stringstream Title;
    if(model.EdgeType==Edge::Zigzag){
        Title << "set title \"Energy bands in honeycomb ribbon with zigzag edges \\n "
              << "N = " << model.N() << " | "
              << "t = " << model.t  << " | "
              << "V = " << model.V << " | "
              << "{/Symbol l} = " << model.lambda;
    } else if(model.EdgeType==Edge::Armchar){
        Title << "set title \"Energy bands in honeycomb ribbon with armchair edges \\n "
              << "N = " << model.N() << " | "
              << "t = " << model.t  << " | "
              << "V = " << model.V << " | "
              << "{/Symbol l} = " << model.lambda;
    }
    gp.sendLine("reset");
    gp.sendLine("unset key");
    gp.sendLine(Title.str());
    gp.sendLine("set xlabel \"k\"");
    gp.sendLine("set ylabel \"E/t\" ");
    gp.sendLine("set xrange[-0.05:2*pi+0.05]");
    gp.sendLine("set yrange[-3.1:3.1]");

    gp.sendLine("set lmargin 10");
    gp.sendLine("set bmargin 5");
    gp.sendLine("set rmargin 10");
    gp.sendLine("set tmargin 10");

    gp.sendLine("set title font 'helvetica,20'");
    gp.sendLine("set xlabel font 'helvetica,20'");
    gp.sendLine("set ylabel font 'helvetica,20'");
    gp.sendLine("set xtics font 'helvetica,14'");
    gp.sendLine("set ytics font 'helvetica,14'");

    gp.sendLine("set xtics (\"0\" 0, \"{/Symbol P}\" pi, \"2{/Symbol P}\" 2*pi)");
}

void replotData(GnuplotPipe& gp, PhisicalModel& model){
    gp.sendLine("set multiplot");
    std::stringstream Title;
    if(model.EdgeType==Edge::Zigzag){
        Title << "set title \"Energy bands in honeycomb ribbon with zigzag edges \\n "
              << "N = " << model.N() << " | "
              << "t = " << model.t  << " | "
              << "V = " << model.V << " | "
              << "{/Symbol l} = " << model.lambda;
    } else if(model.EdgeType==Edge::Armchar){
        Title << "set title \"Energy bands in honeycomb ribbon with armchair edges \\n "
              << "N = " << model.N() << " | "
              << "t = " << model.t  << " | "
              << "V = " << model.V << " | "
              << "{/Symbol l} = " << model.lambda;
    }
    gp.sendLine(Title.str());
    gp.sendLine("plot \"../data/out.data\" u 1:2 w l lc rgb 'blue'");
    for(int iter = 1; iter<model.N(); iter++){
        gp.sendLine("replot \"../data/out.data\" u 1:" + std::to_string(iter+2) + " w l lc rgb 'blue'");
    }
    gp.sendLine("unset multiplot");
    gp.sendEndOfData(1);
}

void recalculateAndSaveToFile(std::vector<KPoint> points, PhisicalModel& model){
    std::fstream file;
    file.open("../data/out.data", std::ios::out);
    for(auto point : points){
        if(model.EdgeType == Edge::Zigzag){
            point.generateHamiltonianWithZigZagEdge();
        } else {
            point.generateHamiltonianWithArmchairEdge();
        }
        point.calculate();
        file << point << std::endl;
    }
    file.close();
}

int main(){
    PhisicalModel Model(8);
    std::vector<KPoint> Points = generateKPointGrid(Model);
    GnuplotPipe gp;


    char command='t';

    while(command!='o'){
        std::cout << "N: " << Model.N() << " | "
                  << "E: " << static_cast<int>(Model.EdgeType) << " | "
                  << "V: " << Model.V << " | "
                  << "L: " << Model.lambda << " |    "
                  << "Command: ";

        std::cin >> command;


        if(command=='q'){ Model.V+=0.01; }
        if(command=='w'){ Model.V+=0.1; }
        if(command=='a'){ Model.V-=0.01; }
        if(command=='s'){ Model.V-=0.1; }

        if(command=='e'){ Model.lambda+=0.01; }
        if(command=='r'){ Model.lambda+=0.1; }
        if(command=='d'){ Model.lambda-=0.01; }
        if(command=='f'){ Model.lambda-=0.1; }

        if(command=='t'){ Model.EdgeType = Edge::Zigzag; }
        if(command=='g'){ Model.EdgeType = Edge::Armchar; }

        if(command=='o'){ break; }


        if(command=='z'){
            recalculateAndSaveToFile(Points, Model);
            setGnuplotData(gp, Model);
            replotData(gp, Model);
        }

        if( std::abs(Model.lambda) < 0.01 ) Model.lambda = 0.0;
        if( std::abs(Model.V) < 0.01 ) Model.V= 0.0;
    }

    return 0;
}

