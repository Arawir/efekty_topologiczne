#include "phisicalmodel.h"

PhisicalModel::PhisicalModel(int numberOfAtoms) :
    t{1.0}
  , lambda{0.0}
  , V{0.0}
  , EdgeType{Edge::Zigzag}
  , Hamiltonian{numberOfAtoms,numberOfAtoms}
  , NumberOfAtoms{numberOfAtoms}
{

}

void PhisicalModel::setHamiltonianCellIfPossible(int row, int column, complex value)
{
    if( (row>=0) && (row<NumberOfAtoms) ){
        if( (column>=0) && (column<NumberOfAtoms) ){
            Hamiltonian(row, column) = value;
        }
    }
}

bool PhisicalModel::hamiltonianIsSelfAdjoint()
{
    Eigen::MatrixXcd tmp= Hamiltonian;
    tmp.adjoint();
    if(Hamiltonian != tmp){
        return false;
    };
    return true;
}

void PhisicalModel::clearHamiltonian()
{
    for(int iter=0; iter<NumberOfAtoms; iter++){
       for(int jter=0; jter<NumberOfAtoms; jter++){
           Hamiltonian(iter,jter) = 0.0;
       }
    }
}

