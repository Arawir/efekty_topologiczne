#ifndef DEFINES
#define DEFINES

#include <iostream>
#include <iomanip>
#include "Eigen/Dense"

#define output std::cout << std::fixed << std::setprecision(4)
#define debug(expr) //std::cout << std::fixed << std::setprecision(4) << expr

#define complex std::complex<double>
#define i complex{0.0,1.0}

#endif // DEFINES

