#include "kpoint.h"
#include <cmath>

KPoint::KPoint(double k, PhisicalModel& model) :
    K{k}
  , Model{model}
{

}


void KPoint::generateHamiltonianWithZigZagEdge()
{
    int row = 0;
    Model.clearHamiltonian();

    while(row<Model.N()){
        Model.setHamiltonianCellIfPossible( row, row-2, i*Model.lambda*( -1.0+exp(-i*K) ));
        Model.setHamiltonianCellIfPossible( row, row-1, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( row, row,   Model.V + i*Model.lambda*( exp(i*K)-exp(-i*K)));
        Model.setHamiltonianCellIfPossible( row, row+1, Model.t*( 1.0+exp(-i*K) ));
        Model.setHamiltonianCellIfPossible( row, row+2, i*Model.lambda*( -1.0+exp(-i*K) ));

        row++;
        if(row>=Model.N()){ break; }

        Model.setHamiltonianCellIfPossible( row, row-2, i*Model.lambda*( -1.0+exp(i*K) ));
        Model.setHamiltonianCellIfPossible( row, row-1, Model.t*( 1.0+exp(i*K) ));
        Model.setHamiltonianCellIfPossible( row, row,   -Model.V + i*Model.lambda*( -exp(i*K)+exp(-i*K)));
        Model.setHamiltonianCellIfPossible( row, row+1, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( row, row+2, i*Model.lambda*( -1.0+exp(i*K) ));

        row++;
        if(row>=Model.N()){ break; }

        Model.setHamiltonianCellIfPossible( row, row-2, i*Model.lambda*( 1.0-exp(i*K) ));
        Model.setHamiltonianCellIfPossible( row, row-1, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( row, row,   Model.V + i*Model.lambda*( exp(i*K)-exp(-i*K)));
        Model.setHamiltonianCellIfPossible( row, row+1, Model.t*( 1.0+exp(i*K) ));
        Model.setHamiltonianCellIfPossible( row, row+2, i*Model.lambda*( 1.0-exp(i*K) ));

        row++;
        if(row>=Model.N()){ break; }

        Model.setHamiltonianCellIfPossible( row, row-2, i*Model.lambda*( 1.0-exp(-i*K) ));
        Model.setHamiltonianCellIfPossible( row, row-1, Model.t*( 1.0+exp(-i*K) ));
        Model.setHamiltonianCellIfPossible( row, row,   -Model.V + i*Model.lambda*( exp(-i*K)-exp(i*K)));
        Model.setHamiltonianCellIfPossible( row, row+1, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( row, row+2, i*Model.lambda*( 1.0-exp(-i*K) ));

        row++;
        if(row>=Model.N()){ break; }
    }

    if( !Model.hamiltonianIsSelfAdjoint() ){
        std::cout << "WARNING | Hamiltonian is not self adjoint!" << std::endl;
    };
}

void KPoint::generateHamiltonianWithArmchairEdge()
{
    int n=0;
    Model.clearHamiltonian();

    while(n<Model.N()){

        Model.setHamiltonianCellIfPossible( n,   n, Model.V*( 1.0 ) );
        Model.setHamiltonianCellIfPossible( n+1, n, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n+3, n, Model.t*( exp(-i*K) ));
        Model.setHamiltonianCellIfPossible( n+7, n, Model.t*( exp(-i*K) ));
        Model.setHamiltonianCellIfPossible( n-4, n, i*Model.lambda*( -1.0 ));
        Model.setHamiltonianCellIfPossible( n+2, n, i*Model.lambda*( 1.0+exp(-i*K) ));
        Model.setHamiltonianCellIfPossible( n+4, n, i*Model.lambda*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n+6, n, i*Model.lambda*( -1.0-exp(-i*K) ));

        Model.setHamiltonianCellIfPossible( n+1, n+1, Model.V*( -1.0 ));
        Model.setHamiltonianCellIfPossible( n+2, n+1, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n+6, n+1, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n,   n+1, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n+3, n+1, i*Model.lambda*( -1.0-exp(-i*K) ));
        Model.setHamiltonianCellIfPossible( n-3, n+1, i*Model.lambda*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n+7, n+1, i*Model.lambda*( 1.0+exp(-i*K) ));
        Model.setHamiltonianCellIfPossible( n+5, n+1, i*Model.lambda*( -1.0 ));

        Model.setHamiltonianCellIfPossible( n+2, n+2, Model.V*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n-3, n+2, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n+3, n+2, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n+1, n+2, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n-4, n+2, i*Model.lambda*( 1.0+exp(i*K) ));
        Model.setHamiltonianCellIfPossible( n-2, n+2, i*Model.lambda*( -1.0 ));
        Model.setHamiltonianCellIfPossible( n,   n+2, i*Model.lambda*( -1.0-exp(i*K) ));
        Model.setHamiltonianCellIfPossible( n+6, n+2, i*Model.lambda*( 1.0 ));

        Model.setHamiltonianCellIfPossible( n+3, n+3, Model.V*( -1.0 ));
        Model.setHamiltonianCellIfPossible( n+2, n+3, Model.t*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n,   n+3, Model.t*( exp(i*K) ));
        Model.setHamiltonianCellIfPossible( n-4, n+3, Model.t*( exp(i*K) ));
        Model.setHamiltonianCellIfPossible( n-3, n+3, i*Model.lambda*( -1.0-exp(i*K) ));
        Model.setHamiltonianCellIfPossible( n-1, n+3, i*Model.lambda*( 1.0 ));
        Model.setHamiltonianCellIfPossible( n+1, n+3, i*Model.lambda*( 1.0+exp(i*K) ));
        Model.setHamiltonianCellIfPossible( n+7, n+3, i*Model.lambda*( -1.0 ));

        n+=4;
    }

    if( !Model.hamiltonianIsSelfAdjoint() ){
        std::cout << "WARNING | Hamiltonian is not self adjoint!" << std::endl;
    };
}

void KPoint::calculate()
{
    Model.EigenSolver.compute(Model.Hamiltonian);
    Eigenvalues = Model.EigenSolver.eigenvalues();
    Eigenvectors = Model.EigenSolver.eigenvectors();
}

std::ostream &operator<<(std::ostream &stream, const KPoint &point)
{
    stream << std::fixed << std::setprecision(6) << std::showpos
           << point.K << "      ";
    for(int iter=0; iter<point.Model.N(); iter++){
        stream << point.Eigenvalues(iter) << "   ";
    }

    return stream;

}
