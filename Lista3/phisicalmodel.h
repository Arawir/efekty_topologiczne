#ifndef PHISICALMODEL_H
#define PHISICALMODEL_H

#include "defines.h"

enum class Edge{
    Zigzag,
    Armchar
};

class PhisicalModel
{
public:
    PhisicalModel(int numberOfAtoms);
    ~PhisicalModel() = default;

    int N(){ return NumberOfAtoms; }
    void setHamiltonianCellIfPossible(int row, int column, complex value);
    bool hamiltonianIsSelfAdjoint();
    void clearHamiltonian();
public:
    double t = 1.0;
    double lambda = 0.2;
    double V = 0.2;
    Edge EdgeType;

    Eigen::MatrixXcd Hamiltonian;
    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> EigenSolver;
private:
    int NumberOfAtoms;
};

#endif // PHISICALMODEL_H
