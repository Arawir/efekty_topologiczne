#include "sshmodel.h"
#include <iomanip>
#include <fstream>
#include <string>

SSHModel::SSHModel() :
    T(1.0)
  , dT(0.0)
{
    for(int n=0; n<100; n++){
        K[n] =-M_PI + static_cast<double>(n)*2.0*M_PI/100.0;
    }
}

void SSHModel::calculate()
{
    for(int n=0; n<100; n++){
        prepareHamiltonian(n);
        calculateEigen();
        calculateEnergyBands(n);
        calculateD(n);
        calculateEigenvectors(n);
    }
    calculateBerryPhase();
}

void SSHModel::setTandDeltaT(double t, double dt)
{
    T = t;
    dT = dt;
}

void SSHModel::saveDataToFile()
{
    std::fstream file("../data/dT="+std::to_string(dT), std::ios::out);

    for(int iter=0; iter<100; iter++){
        file << std::fixed << std::setprecision(4)
             << K[iter].real() << "    "
             << EnergyBands[0][iter].real() << "     "
             << EnergyBands[1][iter].real() << "     "
             << D[0][iter].real() << "     "
             << D[1][iter].real() << "     "
             << D[2][iter].real() << "     "
             << BerryPhase[0] << "     "
             << BerryPhase[1] << std::endl;
    }

    file.close();
}

void SSHModel::saveDataToFile(std::string fileName)
{
    std::fstream file("../data/"+fileName, std::ios::out);

    for(int iter=0; iter<100; iter++){
        file << std::fixed << std::setprecision(4)
             << K[iter].real() << "    "
             << EnergyBands[0][iter].real() << "     "
             << EnergyBands[1][iter].real() << "     "
             << D[0][iter].real() << "     "
             << D[1][iter].real() << "     "
             << D[2][iter].real() << "     "
             << BerryPhase[0] << "     "
             << BerryPhase[1] << std::endl;
    }

    file.close();
}

void SSHModel::prepareHamiltonian(int n)
{
    Hamiltonian(0,0) = 0.0;
    Hamiltonian(0,1) = T+dT+(T-dT)*exp(-i*K[n]);
    Hamiltonian(1,0) = T+dT+(T-dT)*exp(i*K[n]);
    Hamiltonian(1,1) = 0.0;
}

void SSHModel::calculateEigen()
{
    Eigensolver.compute(Hamiltonian);
}

void SSHModel::calculateEnergyBands(int n)
{
    EnergyBands[0][n] = Eigensolver.eigenvalues()[0];
    EnergyBands[1][n] = Eigensolver.eigenvalues()[1];
}

void SSHModel::calculateD(int n)
{
    D[0][n] = T+dT + (T-dT)*cos(K[n]);
    D[1][n] = (T-dT)*sin(K[n]);
    D[2][n] = 0.0;
}

void SSHModel::calculateEigenvectors(int n)
{
    Eigenvectors[0][n] = Eigensolver.eigenvectors().col(0);
    Eigenvectors[1][n] = Eigensolver.eigenvectors().col(1);
}

void SSHModel::calculateBerryPhase()
{
    std::complex<double> tmp1 = 1.0;
    std::complex<double> tmp2 = 1.0;
    for(int n=0; n<99; n++){
        tmp1 = tmp1 * static_cast<std::complex<double>>(Eigenvectors[0][n].adjoint()*Eigenvectors[0][n+1]);
        tmp2 = tmp2 * static_cast<std::complex<double>>(Eigenvectors[1][n].adjoint()*Eigenvectors[1][n+1]);
    }

    if(abs(tmp1) >=0.1)
        BerryPhase[0]=std::arg(tmp1);
    if(abs(tmp2) >=0.1)
        BerryPhase[1]=std::arg(tmp2);
}
