#ifndef SSHMODEL_H
#define SSHMODEL_H

#include <iostream>
#include <cmath>
#include <Eigen/Dense>

#define i std::complex<double>{0,1}
#define eigenvector EigenSolver<Matrix2cd>::EigenvectorsType

using namespace Eigen;
using namespace std;


class SSHModel
{
public:
    SSHModel();
    ~SSHModel() = default;

    void calculate();
    void setTandDeltaT(double t, double dt);
    void saveDataToFile();
    void saveDataToFile(std::string fileName);
private:
    void prepareHamiltonian(int n);
    void calculateEigen();
    void calculateEnergyBands(int n);
    void calculateD(int n);
    void calculateEigenvectors(int n);
    void calculateBerryPhase();

public:
    std::complex<double> K[100];
    std::complex<double> EnergyBands[2][100];
    std::complex<double> D[3][100];
    Vector2cd Eigenvectors[2][100];
    double BerryPhase[2];
private:
    double T;
    double dT;
    Matrix2cd Hamiltonian;
    SelfAdjointEigenSolver<Matrix2cd> Eigensolver;
};

#endif // SSHMODEL_H
