#include <fstream>

#include "sshmodel.h"
#include "finitesshmodel.h"

void saveBerryPhase(){
    SSHModel InfiniteModel;
    std::fstream file;
    file.open("../data/berryPhase", std::ios::out);

    double dT = -1.0;
    while(dT<=1.05){
        InfiniteModel.setTandDeltaT(1.0,dT);
        InfiniteModel.calculate();
        file << dT << "     "
             << InfiniteModel.BerryPhase[0] << "     "
             << InfiniteModel.BerryPhase[1] << std::endl;
        dT+=0.05;
    }

    file.close();
}

int main(){
    SSHModel InfiniteModel;

    InfiniteModel.setTandDeltaT(1.0,-0.2);
    InfiniteModel.calculate();
    InfiniteModel.saveDataToFile();

    InfiniteModel.setTandDeltaT(1.0,0.0);
    InfiniteModel.calculate();
    InfiniteModel.saveDataToFile();

    InfiniteModel.setTandDeltaT(1.0,0.2);
    InfiniteModel.calculate();
    InfiniteModel.saveDataToFile();

    saveBerryPhase();


//    std::fstream file;
//    file.open("../data/N10_N", std::ios::out);
//    FiniteSSHModel FiniteModel;
//    FiniteModel.setTandDeltaT(1.0, -0.1);
//    FiniteModel.setN(10);
//    FiniteModel.calculate();
//    for(int iter=0; iter<20; iter++){
//        std::cout << iter << "    "
//             << FiniteModel.EnergyLevels[iter] << std::endl;
//    }
//    file.close();


    return 0;
}

