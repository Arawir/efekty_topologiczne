#ifndef FINITESSHMODEL_H
#define FINITESSHMODEL_H

#include <iostream>
#include <cmath>
#include <Eigen/Dense>

#define i std::complex<double>{0,1}
#define eigenvector EigenSolver<Matrix2cd>::EigenvectorsType

using namespace Eigen;
using namespace std;

class FiniteSSHModel
{
public:
    FiniteSSHModel();
    ~FiniteSSHModel() = default;

    void calculate();
    void setTandDeltaT(double t, double dt);
    void setN(int n);
private:
    void prepareHamiltonian();
    void calculateEigen();
    void calculateEnergyLevels();
    void calculateDensityOfWaveFunction();
public:
    double T;
    double dT;
    int N;
    Matrix<double,Dynamic,Dynamic> Hamiltonian;
    VectorXd EnergyLevels;
    VectorXd DensityOfWaveFunction[2];
    SelfAdjointEigenSolver<MatrixXd> Eigensolver;
};

#endif // FINITESSHMODEL_H
