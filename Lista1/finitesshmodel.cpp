#include "finitesshmodel.h"
#include <iomanip>
FiniteSSHModel::FiniteSSHModel() :
    T{1.0}
  , dT{0.0}
  , N{2}
{
    Hamiltonian.resize(2*N,2*N);
    EnergyLevels.resize(2*N);
}

void FiniteSSHModel::calculate()
{
    prepareHamiltonian();
    calculateEigen();
    calculateEnergyLevels();
    calculateDensityOfWaveFunction();
}

void FiniteSSHModel::setTandDeltaT(double t, double dt)
{
    T = t;
    dT = dt;
}

void FiniteSSHModel::setN(int n)
{
    N = n;
    Hamiltonian.resize(2*N,2*N);
    EnergyLevels.resize(2*N);
    DensityOfWaveFunction[0].resize(N);
    DensityOfWaveFunction[1].resize(N);
}

void FiniteSSHModel::prepareHamiltonian()
{
    Hamiltonian.setZero();
    for(int n=0; n<2*N-1; n+=2){
        Hamiltonian(n,n+1) = T+dT; //v
        Hamiltonian(n+1,n) = T+dT;
    }
    for(int n=1; n<2*N-1; n+=2){ //w
        Hamiltonian(n,n+1) = T-dT;
        Hamiltonian(n+1,n) = T-dT;
    }
}

void FiniteSSHModel::calculateEigen()
{
    Eigensolver.compute(Hamiltonian);
}

void FiniteSSHModel::calculateEnergyLevels()
{
    for(int n=0; n<2*N; n++){
        EnergyLevels[n] = Eigensolver.eigenvalues()[n];
    }
}

void FiniteSSHModel::calculateDensityOfWaveFunction()
{
    for(int n=0; n<N*2; n++){
//        std::cout << std::fixed << std::setprecision(6)
//                  << n/2+1 << "      "
//                  << Eigensolver.eigenvectors().col(N-1).row(n)*Eigensolver.eigenvectors().col(N-1).row(n) << "      "
//                  << Eigensolver.eigenvectors().col(N).row(n)*Eigensolver.eigenvectors().col(N).row(n) << "      "
//                  << std::endl;
    }
}

