#include <iostream>
#include <vector>
#include <fstream>

#include <kpoint.h>

int main()
{
    PhisicalModel KaneMeleModel(sqrt(3), 1.0, 0.0, 0.0, 1.0);
    std::vector<KPoint> Points;

    double Kx=-4.0;
    double Ky=-4.0;

    while(Kx<4.0){
        Ky = -4.0;
        while(Ky<4.0){
            Points.push_back({Kx, Ky, KaneMeleModel});
            Ky+=0.1;
        }
        Kx+=0.1;
    }

    std::fstream file;
    file.open("../data/EB", std::ios::out);

    for(KPoint& point : Points){
        file << point.Kx << "   "
             << point.Ky << "     "
             << point.Energies[0] << "   "
             << point.Energies[1] << "   "
             << point.Energies[2] << "   "
             << point.Energies[3] << "   "
             << std::endl;
    }

    file.close();

    return 0;
}

