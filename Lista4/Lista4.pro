TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    kpoint.cpp \
    phisicalmodel.cpp \
    defines.cpp

HEADERS += \
    kpoint.h \
    defines.h \
    phisicalmodel.h \
    dirackmatrix.h

DISTFILES +=

