#ifndef DIRACKMATRIX_H
#define DIRACKMATRIX_H

#include <Eigen/Dense>

#define cpl std::complex<double>

class DirackMatrix : public Eigen::Matrix4cd
{
public:
    DirackMatrix(cpl aa, cpl ab, cpl ac, cpl ad,
                 cpl ba, cpl bb, cpl bc, cpl bd,
                 cpl ca, cpl cb, cpl cc, cpl cd,
                 cpl da, cpl db, cpl dc, cpl dd
                 ) : Eigen::Matrix4cd{}
    {
        *this << aa, ab, ac, ad
               , ba, bb, bc, bd
               , ca, cb, cc, cd
               , da, db, dc, dd;
    }
};

#endif // DIRACKMATRIX_H
