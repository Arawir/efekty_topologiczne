#ifndef PHISICALMODEL_H
#define PHISICALMODEL_H

#include "defines.h"

class PhisicalModel
{
public:
    PhisicalModel(double a, double t, double lso, double lr, double v);
    ~PhisicalModel() = default;

    Eigen::MatrixXcd generateHamiltonian(double kx, double ky);
    Eigen::VectorXd eigenvalues(Eigen::MatrixXcd hamiltonian);
    Eigen::MatrixXcd eigenvectors(Eigen::MatrixXcd hamiltonian);
public:
    double A;
    double T;
    double Lso;
    double Lr;
    double V;
    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> EigenSolver;
};

#endif // PHISICALMODEL_H
