#include "defines.h"

DirackMatrix G1{0,0,1,0,
                0,0,0,1,
                1,0,0,0,
                0,1,0,0};

DirackMatrix G2{1,0,0,0,
                0,1,0,0,
                0,0,-1,0,
                0,0,0,-1};

DirackMatrix G3{0,0,0,-i,
                0,0,-i,0,
                0,i,0,0,
                i,0,0,0};

DirackMatrix G4{0,0,0,-1,
                0,0,1,0,
                0,1,0,0,
                -1,0,0,0};



DirackMatrix G12{0,0,-i,0,
                 0,0,0,-i,
                 i,0,0,0,
                 0,i,0,0};

DirackMatrix G15{1,0,0,0,
                 0,-1,0,0,
                 0,0,-1,0,
                 0,0,0,1};

DirackMatrix G23{0,0,0,-1,
                 0,0,-1,0,
                 0,-1,0,0,
                 -1,0,0,0};

DirackMatrix G24{0,0,0,i,
                 0,0,-i,0,
                 0,i,0,0,
                 -i,0,0,0};


