#include "kpoint.h"

KPoint::KPoint(double kx, double ky, PhisicalModel &model) :
    Kx{kx}
  , Ky{ky}
  , Model{model}
{
    Hamiltonian = Model.generateHamiltonian(Kx, Ky);
    Energies = Model.eigenvalues(Hamiltonian);
    EigenVectors = Model.eigenvectors(Hamiltonian);
}
