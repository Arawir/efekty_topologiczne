#ifndef DEFINES
#define DEFINES

#include <iostream>
#include <iomanip>
#include "Eigen/Dense"
#include "dirackmatrix.h"

#define output std::cout << std::fixed << std::setprecision(4)
#define debug(expr) //std::cout << std::fixed << std::setprecision(4) << expr

#define complex std::complex<double>
#define i complex{0.0,1.0}


extern DirackMatrix G1;
extern DirackMatrix G2;
extern DirackMatrix G3;
extern DirackMatrix G4;
extern DirackMatrix G12;
extern DirackMatrix G15;
extern DirackMatrix G23;
extern DirackMatrix G24;

#endif // DEFINES

