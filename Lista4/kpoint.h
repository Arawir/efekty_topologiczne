#ifndef KPOINT_H
#define KPOINT_H

#include "phisicalmodel.h"

class KPoint
{
public:
    KPoint(double kx, double ky, PhisicalModel& model);
    ~KPoint() = default;
public:

    double Kx, Ky;
    Eigen::MatrixXcd Hamiltonian;
    Eigen::VectorXd Energies;
    Eigen::MatrixXcd EigenVectors;

    PhisicalModel& Model;
};

#endif // KPOINT_H
