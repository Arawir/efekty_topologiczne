#include "phisicalmodel.h"

PhisicalModel::PhisicalModel(double a, double t, double lso, double lr, double v) :
    A{a}, T{t}, Lso{lso}, Lr{lr}, V{v}
{

}

Eigen::VectorXd PhisicalModel::eigenvalues(Eigen::MatrixXcd hamiltonian)
{
    EigenSolver.compute(hamiltonian);
    return EigenSolver.eigenvalues();
}

Eigen::MatrixXcd PhisicalModel::eigenvectors(Eigen::MatrixXcd hamiltonian)
{
    EigenSolver.compute(hamiltonian);
    return EigenSolver.eigenvectors();
}

Eigen::MatrixXcd PhisicalModel::generateHamiltonian(double kx, double ky)
{
    double x = kx*A/2.0;
    double y = ky*A*sqrt(3.0)/2.0;
    Eigen::MatrixXcd Hamiltonian;

    Hamiltonian = T*(1.0+2.0*cos(x)*cos(y))*G1;
    Hamiltonian += V*G2;
    Hamiltonian += Lr*(1.0-cos(x)*cos(y))*G3;
    Hamiltonian += -sqrt(3.0)*Lr*sin(x)*sin(y)*G4;

    Hamiltonian += -2.0*T*cos(x)*sin(y)*G12;
    Hamiltonian += Lso*(2.0*sin(2.0*x) - 4.0*sin(x)*cos(y))*G15;
    Hamiltonian += -Lr*cos(x)*sin(y)*G23;
    Hamiltonian += sqrt(3.0)*Lr*sin(x)*cos(y)*G24;

    return Hamiltonian;
}

